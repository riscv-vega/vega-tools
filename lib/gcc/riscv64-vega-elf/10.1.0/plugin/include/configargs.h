/* Generated automatically. */
static const char configuration_arguments[] = "/home/hdg/Desktop/desktop/toolchain/riscv-gnu-toolchain/riscv-gcc/configure --target=riscv64-vega-elf --prefix=/home/hdg/Desktop/toolchain/riscv --disable-shared --disable-threads --enable-languages=c,c++ --with-system-zlib --enable-tls --with-newlib --with-sysroot=/home/hdg/Desktop/toolchain/riscv/riscv64-vega-elf --with-native-system-header-dir=/include --disable-libmudflap --disable-libssp --disable-libquadmath --disable-libgomp --disable-nls --disable-tm-clone-registry --src=.././riscv-gcc --enable-multilib --with-abi=lp64d --with-arch=rv64imafdc --with-tune=rocket 'CFLAGS_FOR_TARGET=-Os   -mcmodel=medany' 'CXXFLAGS_FOR_TARGET=-Os   -mcmodel=medany'";
static const char thread_model[] = "single";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "abi", "lp64d" }, { "arch", "rv64imafdc" }, { "tune", "rocket" } };
